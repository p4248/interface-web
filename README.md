# Projet de plateforme de challenges en cybersécurité #

## Sommaire
  - [Sommaire](#sommaire)
  - [Contexte du projet](#contexte-du-projet)
  - [Installer un IDE](#installer-un-ide)
  - [Installer Git](#installer-git)
  - [Installer Node.js et npm](#installer-nodejs-et-npm)
  - [Installer PHP](#installer-php)
  - [Installer Composer](#installer-composer)
  - [Configurer & lancer le projet](#configurer--lancer-le-projet)

## Contexte du projet
Ce projet nous a été commandé par l'école ESIEE-IT pour notre projet de fin d'année.
Il s'agit donc ici de réaliser une plateforme permettant de créer des challenges de type CTF à disposition des étudiants de l'école pour les futures années.

Nous avons choisi de réaliser ce projet en Laravel/VueJS, des frameworks adaptés et puissants pour réaliser des SPA entre autres.

## Installer un IDE
L'IDE est au choix libre de chaque développeur, mais il est conseillé d'utiliser [VS Code](https://code.visualstudio.com/) qui propose pas mal de plugins intégrés facilitant le dév. Parmi ces fonctions, on retrouve les plugins de linting pour le code, mais aussi des plugins d'autocomplétion, ainsi qu'une console intégré permettant le lancement du projet directement dans l'IDE.

De plus, VS Code est assez largement utilisé et plutôt light par rapport à ses compères de la suite JetBrains. Il suffire juste d'ouvrir le dossier du projet importé et de configurer en fonction des prochaines steps pour que le projet fonctionne en local.


## Installer Git
Pour installer Git s'il ne l'est pas déjà, il faut passer par le [Git GUI](https://git-scm.com/downloads) pour Windows.
Il faut penser à sélectionner l'option de CLI lors de l'installaton :
<img src="https://phoenixnap.com/kb/wp-content/uploads/2021/04/adjust-git-path-enviorment.png" width=60%>

Si vous ne sélectionnez pas cette option, bvous ne pourrez utiliser les commandes Git seulement via le Git Bash plutôt qu'en CLI, et notamment sur la CLI intégrée à VS Code qui vous fera gagner un peu de temps.


## Installer Node.js et npm
Pour installer Node.js qui permettra de disposer de npm, il faut récupérer [l'installer Windows](https://nodejs.org/en/). Rien de spécial sur l'installer, mais pour vérifier la bonne installation, lancez la commande qui doit vous retourner la version installée de npm : 

```npm -v```

## Installer PHP
Si vous disposez de Wamp/Xampp, PHP est déjà installé sur votre machineµ. Il se peut néanmoins que PHP n'ait pas été ajouté aux variables d'environnements (détail ci-dessous).

PHP peut s'installer de deux manières différentes :
- Soit via [Wamp/Xampp](https://www.wampserver.com/en/) qui installe à la fois des versions de PHP, de MySQL et d'Apache en all-in-one
- Soit en [téléchargeant le zip](https://windows.php.net/download) de la version PHP la plus récente. Dans ce cas, il faut extraire le zip dans un répertoire */php* sur votre machine

Une fois PHP à votre disposition, vous devrez l'ajouter aux variables d'environnement. Pour ce faire, vous devez accéder aux propriétés de votre PC, puis ajouter une variable au *PATH* :

Ce PC > Propriétés > Paramètres avancés du système > Variables d'environnement

Vous sélectionnez *PATH* ou *Path* puis vous cliquez sur *Modifier ...*. Vous ajoutez ensuite une variable en renseignant le chemin vers votre dossier PHP. Vous testez ensuite le fonctionnement de PHP :

```php -v```

## Installer Composer
Pour installer Composer, il faut récupérer le [setup d'installation Windows](https://getcomposer.org/doc/00-intro.md#globally). Une fois installé, vérifiez l'installation avec la commande suivante :

```composer -v```
## Configurer & lancer le projet
Une fois toutes les installations réussies, il faut configurer certains points avant de pouvoir lancer le projet. 

Tout d'abord, il faut cloner le repo dans votre machine :

```git clone https://gitlab.com/p4248/interface-web.git```

Une fois cloné, vous devez configurer votre profil pour les futures modifications sur le projet :

```git config --global user.name "Prénom NOM"```

```git config --global user.email "email@email.com" ```

Une fois configuré, vous pouvez *cd* dans le répertoire de projet, puis lancer les commandes suivantes pour installer toutes les dépendances :

```npm install```

```composer install```

```composer update```

Vous êtes enfin prêts à lancer le projet. Renommer le fichier .env.example en .env et demandez les valeurs à changer aux autres membres de l'équipe puis lancez le projet avec les commandes suivantes dans deux terminaux différents :

```php artisan serve```

```npm run dev```

Une fois lancée, vous avez accès sur http://127.0.0.1:3000 à votre serveur local de dév.
