<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChallengeController;
use App\Http\Controllers\ChallengeUserController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\MachineController;
use App\Http\Controllers\ChallengeMachineController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/challenge', [ChallengeController::class, 'getCurrentChall']);
    Route::post('/create-challenge', [ChallengeController::class, 'createChall']);
    Route::post('/challenge-user', [ChallengeUserController::class, 'getUsersByChall']);
    Route::post('/teams', [TeamController::class, 'getTeams']);
    Route::post('/machines', [ChallengeMachineController::class, 'getMachineByChallId']);
    Route::post('/update-user', [UserController::class, 'updateUser']);
    Route::post('/import-user', [UserController::class, 'import']);
    Route::post('/students', [UserController::class, 'getStudents']);
    Route::post('/delete-user', [ChallengeUserController::class, 'deleteUser']);
});

Route::post('/login', [AuthController::class, 'login']);
Route::post('/send-token', [AuthController::class, 'sendToken']);
Route::post('/validate-token', [AuthController::class, 'validateToken']);
Route::post('/reset-password', [AuthController::class, 'resetPassword']);