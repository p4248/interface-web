<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email</title>
</head>
<body>
    <h1>Plateforme - Nouveau mot de passe</h1>
    <p>Bonjour, vous avez demandé la réinitialisation de votre mot de passe pour accéder à la plateforme de challenge en cybersécurité de l'école.</p>
    <p>Voici votre token de réinitialisation à valider sur la page de réinitialisation : <strong>{{ $token }}</strong></p>
    <p>Ce token expirera dans 15 minutes.</p>
</body>
</html>