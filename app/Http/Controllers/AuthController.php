<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Testing\Fluent\Concerns\Has;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Password as Pass;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPassword;
use App\Models\PasswordReset as PReset;
use App\Models\User;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email|string|exists:app_user,email',
            'password' => ['required']
        ]);

        if (!Auth::attempt($credentials)) {
            return response([
                'errors' => ['creds' => 'Mauvais email ou mot de passe.']
            ], 422);
        }
        $user = Auth::user();
        $token = $user->createToken('main')->plainTextToken;

        return response([
            'user' => $user,
            'token' => $token
        ]);
    }

    public function logout()
    {
        $user = Auth::user();
        $user->currentAccessToken()->delete();
        return response([
            'success' => true
        ]);
    }
    
    public function sendToken(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email|string|exists:app_user,email'
        ]);

        $user = User::where('email', $request->email)->first();

        $token = Str::random(32);

        Mail::to($user)->send(new ResetPassword($token));

        $passwordReset = new PReset();
        $passwordReset->user_id = $user->id;
        $passwordReset->valid_for = 15000;
        $passwordReset->token = $token;
        $passwordReset->save();
    }
    
    public function validateToken(Request $request) 
    {
        $credentials = $request->validate([
            'email' => 'required|email|string|exists:app_user,email',
            'token' => 'required|string|exists:reset,token'
        ]);

        $passwordReset = PReset::where('token', $request->token)->first();        
        $user = User::where('id', $passwordReset->user_id)->first();

        // Vérifie que le token appartient à l'email rentré
        if (!($user->email == $request->email)) {
            return response([
                'errors' => ['creds' => 'Mauvais token.']
            ], 422);
        }

        $currentDateTime = Carbon::now();
        $createdAt = Carbon::parse($passwordReset->created_at);

        // Vérifie que le token est toujours valide
        if ($currentDateTime->diffInSeconds($createdAt) > $passwordReset->valid_for) {
            return response([
                'errors' => ['creds' => 'Token expiré.']
            ], 422);
        }

        return response()->json($user, 200);
    }
    
    public function resetPassword(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        
        $user->password = Hash::make($request->newPass);
        $user->update();

        $passwordReset = PReset::where('user_id', $user->id)->first();
        $passwordReset->delete();
    }

}