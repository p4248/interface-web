<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Challenge;
use Carbon\Carbon;

class ChallengeController extends Controller
{
    public function getCurrentChall()
    {
        $challs = Challenge::all();
        
        foreach ($challs as $chall) {
            $currentDateTime = Carbon::now();
            $endDate = Carbon::parse($chall->challenge_end_date);

            if ($currentDateTime->lt($endDate)) {
                return response()->json($chall, 200);
            }
        }     

        return response()->json(NULL, 200);
    }
    
    public function createChall(Request $request)
    {
        $chall = $request->validate([
            'name' => 'required|string',
            'desc' => 'required|string',
            'endDate' => 'required|date|after:today'
        ]);
        $currentDateTime = Carbon::now();

        $challenge = new Challenge;
        $challenge->challenge_name = $request->name;
        $challenge->challenge_description = $request->desc;
        $challenge->challenge_start_date = $currentDateTime;
        $challenge->challenge_end_date = $request->endDate;
        $challenge->save();
    }
}
