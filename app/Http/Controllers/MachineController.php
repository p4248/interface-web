<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Machine;

class MachineController extends Controller
{
    public function getMachine()
    {
        $machine = Machine::all();
        return response()->json($machine, 200);
    }
}
