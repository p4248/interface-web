<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Team;

class TeamController extends Controller
{
    public function getTeams()
    {
        $team = Team::all();
        return response()->json($team, 200);
    }
}
