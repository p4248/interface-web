<?php

namespace App\Http\Controllers;

use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function updateUser(Request $request)
    {
        $user = $request;

        User::where('id', $user->userId)->update([$user->col => $user->newVal]);

        return response()->json($user, 200);
    }

    public function import(Request $request) 
    {
        $filename = $request[0];
        //Excel::import(new UsersImport, $filename);
        //return redirect('/users')->with('success', 'Importé !');

        return response()->json($filename, 200);
    }
    
    public function getStudents() 
    {
        $users = User::where('is_student', 1)->get();
        return response()->json($users, 200);
    }
}
