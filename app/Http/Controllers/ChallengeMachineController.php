<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ChallengeMachine;
use App\Models\Machine;

class ChallengeMachineController extends Controller
{
    public function getMachineByChallId(Request $request)
    {
        $challId = $request->id;
        
        if (!($challId == NULL)) {
            $machinesId = ChallengeMachine::where('challenge_id', $challId)->pluck('machine_id');
            $machines = Machine::whereIn('id', $machinesId)->get();
            return response()->json($machines, 200);
        }

        return response()->json(0, 200);
    }
}
