<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ChallengeUser;
use App\Models\User;

class ChallengeUserController extends Controller
{
    public function getUsersByChall(Request $request)
    {
        $challId = $request->id;
        
        if (!($challId == NULL)) {
            $usersId = ChallengeUser::where('challenge_id', $challId)->pluck('user_id');
            $users = User::whereIn('id', $usersId)->get();
            return response()->json($users, 200);
        }

        return response()->json(0, 200);
    }

    public function deleteUser(Request $request)
    {
        $usersId = $request;

        if (!($usersId == NULL)) {
            $res = ChallengeUser::whereIn('user_id',$usersId)->delete();
            return response()->json($res, 200);
        }
        return response()->json(0, 200);
    }
}
