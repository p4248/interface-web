<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'name'     => $row[0],
            'surname'    => $row[1], 
            'email'    => $row[2], 
            'vpn_pass'    => $row[3], 
            'team_id'    => $row[4], 
        ]);
    }
}
