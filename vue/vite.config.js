import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'



// https://vitejs.dev/config/
export default defineConfig({
  server: {
    proxy: 'http://localhost:8000',
  },
  plugins: [vue()]
})
