import {createStore} from "vuex";
import axiosClient from "../axios";

const store = createStore({
    state: {
        team: {
            loading: false,
            data: {},
        },
        machine: {
            loading: false,
            data: {},
        },
        user: {
            data: {},
            token: sessionStorage.getItem("TOKEN"),
        },
        users: {
            data: {},
        },
        challenge: {
            loading: false,
            users: {},
            data: {}
        }
    },
    getters: {},
    actions: {
        updateUser({ commit }, user) {
            return axiosClient.post('/update-user', user)
            .then(({data}) => {
                commit('updateChallUsers', user)
                return data;
            })
        },
        deleteUser({ commit }, userId) {
            return axiosClient.post('/delete-user', userId)
            .then(({data}) => {
                commit('deleteChallUsers', userId)
                return data;
            })
        },
        login({ commit }, user) {
            return axiosClient.post('/login', user)
            .then(({data}) => {
                commit('setUser', data.user);
                commit('setToken', data.token);
                return data;
            })
        },
        getUser({commit}) {
            return axiosClient.get('/user')
            .then(res => {
                commit('setUser', res.data)
                return res.data;
            })
        },
        logout({ commit }, user) {
            return axiosClient.post('/logout')
            .then(response => {
                commit('logout');
                return response;
            })
        },
        sendToken({ commit }, user) {
            return axiosClient.post('/send-token', user)
            .then(({data}) => {
                return data;
            })
        },
        validateToken({ commit }, user) {
            return axiosClient.post('/validate-token', user)
            .then(({data}) => {
                return data;
            })
        },
        resetPassword({ commit }, user) {
            return axiosClient.post('/reset-password', user)
            .then(({data}) => {
                return data;
            })
        },
        getCurrChallenge({commit}) {
            commit('challLoading', true)

            return axiosClient.get('/challenge')
            .then(res => {
                commit('challLoading', false)
                commit('setChallData', res.data)
                return res;
            })
            .catch(error => {
                commit('challLoading', false)
                return error;
              })
        },
        getUserByChall({commit}, challenge) {
            commit('challLoading', true);
            
            return axiosClient.post('/challenge-user', challenge)
            .then(res => {
                commit('challLoading', false)
                commit('setUserChallData', res.data)
                return res;
            })
            .catch(error => {
                commit('challLoading', false)
                return error;
              })
        },
        getTeams({commit}) {
            commit('teamLoading', true);
            
            return axiosClient.post('/teams')
            .then(res => {
                commit('teamLoading', false)
                commit('setTeamData', res.data)
                return res;
            })
            .catch(error => {
                commit('teamLoading', false)
                return error;
              })
        },
        getMachine({commit}, challenge) {
            commit('machineLoading', true);
            
            return axiosClient.post('/machines', challenge)
            .then(res => {
                commit('machineLoading', false)
                commit('setMachineData', res.data)
                return res;
            })
            .catch(error => {
                commit('machineLoading', false)
                return error;
              })
        },
        importUsers({commit}, file) {
            return axiosClient.post('/import-user', file[0])
            .then(res => {
                return res;
            })
            .catch(error => {
                return error;
              })
        },
        createChall({commit}, chall) {
            return axiosClient.post('/create-challenge', chall)
            .then(res => {                
                commit('setChallData', res);
                return res;
            })
        },
        getStudents({commit}) {
            return axiosClient.post('/students')
            .then(res => {
                commit('setUsers', res.data);
                return res;
            })
        },
    },
    mutations: {
        logout: (state) => {
            state.user.token = null;
            state.user.data = {};
            sessionStorage.removeItem("TOKEN");
        },
        setUsers: (state, users) => {
            state.users.data = users;
        },
        setUser: (state, user) => {
            state.user.data = user;
        },
        setToken: (state, token) => {
            state.user.token = token;
            sessionStorage.setItem('TOKEN', token);
        },
        challLoading: (state, loading) => {
            state.challenge.loading = loading;
        },
        teamLoading: (state, loading) => {
            state.team.loading = loading;
        },
        machineLoading: (state, loading) => {
            state.machine.loading = loading;
        },
        setChallData: (state, chall) => {
            state.challenge.data = chall;
        },
        setUserChallData: (state, users) => {
            state.challenge.users = users;
        },
        setTeamData: (state, team) => {
            state.team.data = team;
        },
        setMachineData: (state, machine) => {
            state.machine.data = machine;
        },
        updateChallUsers: (state, userChall) => {
            //for (var i = 0; i < state.challenge.users.data.length; i++) {
                //if (state.challenge.users.data[i].id == userChall.userId && )
            //}
            //state.challenge.users. = machine;
        },
        deleteChallUsers: (state, user) => {
            for (var i = 0; i < state.challenge.users.length; i++) {
                if (state.challenge.users[i].id == user.userId) state.challenge.users[i] = null;
            }
        },
    },
    modules: {}
})

export default store;