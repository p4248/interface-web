import { createApp } from 'vue'
import store from './store'
import router from './router'
import App from './App.vue'
import VueApexCharts from 'vue3-apexcharts'

createApp(App)
.use(VueApexCharts)
.use(router)
.use(store)
.mount('#app')
