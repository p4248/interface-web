import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Machines from "../views/Machines.vue";
import Users from "../views/Users.vue";
import Score from "../views/Score.vue";
import ForgottenPassword from "../views/ForgottenPassword.vue";
import Navbar from "../components/Navbar.vue";
//import NotFound from "../views/NotFound.vue";
import AuthLayout from "../components/AuthLayout.vue";
import store from "../store";

const routes = [
    {   // Routes avec authent
        path: "/",
        redirect: "/home",
        component: Navbar,
        meta: {requiresAuth: true},
        children: [
            { path: '/home', name: 'Home', component: Home },
            { path: '/machines', name: 'Machines', component: Machines },
            { path: '/users', name: 'Users', component: Users },
        ]
    },
    {   //Si on a des routes accessibles sans login, à ajouter ici
        path: "/auth",
        redirect: "/login",
        name: "Auth",
        component: AuthLayout,
        meta: {isGuest: true},
        children: [
          { path: "/login", name: "Login", component: Login },
          { path: "/forgot", name: "ForgottenPassword", component: ForgottenPassword },
          { path: "/score", name: "Score", component: Score },
        ],
      },
]
const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth && !store.state.user.token) {
    next({ name: "Login" });
  } else if (store.state.user.token && to.meta.isGuest) {
    next({ name: "Home" });
  } else {
    next();
  }
});

export default router;