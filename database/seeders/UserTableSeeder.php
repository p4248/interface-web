<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // Let's clear the users table first
        User::truncate();

        $faker = \Faker\Factory::create();

        // Let's make sure everyone has the same password and 
        // let's hash it before the loop, or else our seeder 
        // will be too slow.
        $password = Hash::make('toptal');

        User::create([
            'app_user_name' => 'Nom',
            'app_user_surname' => 'Prénom',
            'app_user_email' => 'admin@test.com',
            'app_user_is_admin' => true,
            'app_user_created_at' => now(),
            'app_user_vpn_pass' => 'pass',
            'app_user_password' => $password,
        ]);

        // And now let's generate a few dozen users for our app:
        for ($i = 0; $i < 10; $i++) {
            User::create([
                'app_user_name' => $faker->name(),
                'app_user_surname' => $faker->name(),
                'app_user_email' => $faker->email(),
                'app_user_is_admin' => $faker->boolean(),
                'app_user_created_at' => now(),
                'app_user_vpn_pass' => $faker->randomNumber(),
                'app_user_password' => $password,
            ]);
        }
    }
}
