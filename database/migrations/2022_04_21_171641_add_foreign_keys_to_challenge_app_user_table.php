<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToChallengeAppUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('challenge_app_user', function (Blueprint $table) {
            $table->foreign(['challenge_id'], 'challenge_app_user_ibfk_1')->references(['id'])->on('challenge');
            $table->foreign(['user_id'], 'challenge_app_user_ibfk_2')->references(['id'])->on('app_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('challenge_app_user', function (Blueprint $table) {
            $table->dropForeign('challenge_app_user_ibfk_1');
            $table->dropForeign('challenge_app_user_ibfk_2');
        });
    }
}
