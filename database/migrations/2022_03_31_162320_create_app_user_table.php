<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_user', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('app_user_name');
            $table->string('app_user_surname');
            $table->string('app_user_email')->unique('app_user_email');
            $table->string('app_user_password')->nullable();
            $table->boolean('app_user_is_admin')->nullable();
            $table->timestamp('app_user_created_at')->useCurrentOnUpdate()->useCurrent();
            $table->string('app_user_vpn_pass')->nullable()->unique('app_user_vpn_pass');
            $table->integer('team_id')->nullable()->index('team_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_user');
    }
}
