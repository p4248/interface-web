<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlagMachineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flag_machine', function (Blueprint $table) {
            $table->integer('machine_id');
            $table->integer('flag_id')->index('flag_id');
            $table->integer('flag_machine_point');

            $table->primary(['machine_id', 'flag_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flag_machine');
    }
}
