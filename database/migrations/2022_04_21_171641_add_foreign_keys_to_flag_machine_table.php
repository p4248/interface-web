<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToFlagMachineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flag_machine', function (Blueprint $table) {
            $table->foreign(['machine_id'], 'flag_machine_ibfk_1')->references(['id'])->on('machine');
            $table->foreign(['flag_id'], 'flag_machine_ibfk_2')->references(['id'])->on('flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flag_machine', function (Blueprint $table) {
            $table->dropForeign('flag_machine_ibfk_1');
            $table->dropForeign('flag_machine_ibfk_2');
        });
    }
}
