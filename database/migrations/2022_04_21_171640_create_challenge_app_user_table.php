<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChallengeAppUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenge_app_user', function (Blueprint $table) {
            $table->integer('challenge_id');
            $table->integer('user_id')->index('user_id');
            $table->boolean('challenge_app_user_is_etudiant')->nullable();

            $table->primary(['challenge_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenge_app_user');
    }
}
