<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToChallengeMachineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('challenge_machine', function (Blueprint $table) {
            $table->foreign(['machine_id'], 'challenge_machine_ibfk_1')->references(['id'])->on('machine');
            $table->foreign(['challenge_id'], 'challenge_machine_ibfk_2')->references(['id'])->on('challenge');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('challenge_machine', function (Blueprint $table) {
            $table->dropForeign('challenge_machine_ibfk_1');
            $table->dropForeign('challenge_machine_ibfk_2');
        });
    }
}
