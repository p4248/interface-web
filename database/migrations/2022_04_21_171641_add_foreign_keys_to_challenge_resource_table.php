<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToChallengeResourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('challenge_resource', function (Blueprint $table) {
            $table->foreign(['challenge_id'], 'challenge_resource_ibfk_1')->references(['id'])->on('challenge');
            $table->foreign(['resource_id'], 'challenge_resource_ibfk_2')->references(['id'])->on('resource');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('challenge_resource', function (Blueprint $table) {
            $table->dropForeign('challenge_resource_ibfk_1');
            $table->dropForeign('challenge_resource_ibfk_2');
        });
    }
}
